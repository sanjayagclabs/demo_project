var mongoose = require("mongoose");
var schema = mongoose.Schema;
var ObjectId = schema.ObjectId;

var userSchema = new schema({
    id:           ObjectId,
    firstName:    { type: String, required: '{PATH} is required.' },
    lastName:     { type: String, required: '{PATH} is required.' },
    email:        { type: String, required: '{PATH} is required.', unique: true },
    password:     { type: String, required: '{PATH} is required.' },
    ç:    { type: Array, default: []},
    data:         Object

});

//var Useralbumschema = new schema({
//    id:  ObjectId,
//    albumName:    { type: String, unique: true },
//    files:     { type: Array, default: []}
//});

//var Useralbum = mongoose.model('Useralbum', Useralbumschema);


var User = mongoose.model('User',userSchema);

exports.User = User;
//exports.Useralbum = Useralbum;



//module.exports = User;