var mongoose = require("mongoose");
var schema = mongoose.Schema;
var ObjectId = schema.ObjectId;

var Useralbumschema = new schema({
    id:           ObjectId,
    albumName:    { type: String },
    //email:
    file:     { type: Array, default: []}
});

var Useralbum = mongoose.model('Useralbum', Useralbumschema);
exports.Useralbum = Useralbum;