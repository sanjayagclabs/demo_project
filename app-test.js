var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var sessions = require('client-sessions');
var controllers = require('./controllers');
var bcrypt = require('bcryptjs');
var flash = require('express-flash');
var async = require("async");    // async module
var request = require("request");     // request module
var fs = require("fs");         // fs module
var app = express();
app.set('view engine', 'jade');
app.locals.pretty = true;
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
//connect to mongo
mongoose.connect('mongodb://localhost/newauth1');
var user = require('./model/User');
var User = user.User;
var album = require('./model/Useralbum');
var Useralbum = album.Useralbum;
//var user = require('./route/route');

app.use(flash());

//middleware------------------------------------------------------------------------------
app.use(bodyParser.urlencoded({extended: true}));

app.use(sessions({
    cookieName: 'session',
    secret: 'dfjkytfghvjlkgjggcghcghcjgh',
    duration: 30 * 60 * 1000,
    activeDuration: 5 * 60 * 1000
}));

app.use(function (req, res, next) {
    if (req.session && req.session.user) {
        User.findOne({email: req.session.user.email}, function (err, user) {
            if (user) {
                req.user = user;
                delete req.user.password;
                res.locals.user = req.user;
            }
            next();
        });
    }
    else {
        next();
    }
});

function requireLogin(req, res, next) {
    if (!req.user) {
        res.redirect('/test-login');
    } else {
        next();
    }
}

app.get('/', function (req, res) {
    res.render('test-index1.jade');
});
app.get('/test-register', function (req, res) {
    res.render('test-register.jade');
});
app.get('/test-login', function (req, res) {
    res.render('test-login.jade');
});
app.get('/test-dashboard', requireLogin, function (req, res) {
    res.render('test-dashboard.jade');
});
app.get('/addnotes', requireLogin, function (req, res) {
    res.render('addnotes.jade');
});
app.get('/viewnotes', function (req, res) {
    res.render('viewnotes.jade');
});
app.get('/logout', function (req, res) {
    req.session.reset();
    res.redirect('/');
});

app.post('/test-register', function (req, res) {
    //var salt = bcrypt.genSaltSync(10);
    var hash = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10));

    var user = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: hash
    });
    user.save(function (err) {
        if (err) {
            var error = 'Something bad happened! Please try again.';

            if (err.code === 11000) {
                error = 'That email is already taken, please try another.';
            }

            res.render('test-register.jade', {error: error});
        } else {
            res.redirect('/test-dashboard');
        }
    });
});

app.post('/test-login', function (req, res) {
    User.findOne({email: req.body.email}, function (err, user) {
        if (!user) {
            res.render('test-login.jade', {error: "Incorrect email / password."});
        } else {
            if (bcrypt.compareSync(req.body.password, user.password)) {
                req.session.user = user;
                req.session.album = {
                    albumName: 'Shahab'
                };
                res.redirect('/test-dashboard');
            } else {
                res.render('test-login.jade', {error: "Incorrect email / password."});
            }
        }
    });
});

//image upload----------------------------------------------------------------------------
app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/images'));
//app.use('/images', express.static(__dirname + '/public'));

//Define routes
app.get('/index', requireLogin, controllers.index);
app.post('/upload', multipartMiddleware);
app.post('/upload', controllers.upload);


app.post('/addnotes', function (req, reply) {

    var New_Note = {
        NoteName: req.body.NoteName,
        NoteContent: req.body.NoteContent,
        UserName: sessions.firstName,
        Date: new Date()
    };

    New_Note.save(function (err) {

        if (err) {
            var error = 'Something bad happened! Please try again.';

            res.render('addnotes.jade', {error: error});
        } else {
            res.redirect('/viewnodes');
        }

    });
});


app.get('/forgot', function (req, res) {
    res.render('forgot', {
        user: req.user
    });
});

app.post('/forgot', function (req, res, next) {
    async.waterfall([
        function (done) {
            crypto.randomBytes(20, function (err, buf) {
                var token = buf.toString('hex');
                done(err, token);
            });
        },
        function (token, done) {
            User.findOne({email: req.body.email}, function (err, user) {
                if (!user) {
                    req.flash('error', 'No account with that email address exists.');
                    return res.redirect('/forgot');
                }

                user.resetPasswordToken = token;
                user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

                user.save(function (err) {
                    done(err, token, user);
                });
            });
        },
        function (token, user, done) {
            var smtpTransport = nodemailer.createTransport('SMTP', {
                service: 'SendGrid',
                auth: {
                    user: '!!! YOUR SENDGRID USERNAME !!!',
                    pass: '!!! YOUR SENDGRID PASSWORD !!!'
                }
            });
            var mailOptions = {
                to: user.email,
                from: 'passwordreset@demo.com',
                subject: 'Node.js Password Reset',
                text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
                'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
                'http://' + req.headers.host + '/reset/' + token + '\n\n' +
                'If you did not request this, please ignore this email and your password will remain unchanged.\n'
            };
            smtpTransport.sendMail(mailOptions, function (err) {
                req.flash('info', 'An e-mail has been sent to ' + user.email + ' with further instructions.');
                done(err, 'done');
            });
        }
    ], function (err) {
        if (err) return next(err);
        res.redirect('/forgot');
    });
});

app.get('/reset/:token', function (req, res) {
    User.findOne({resetPasswordToken: req.params.token, resetPasswordExpires: {$gt: Date.now()}}, function (err, user) {
        if (!user) {
            req.flash('error', 'Password reset token is invalid or has expired.');
            return res.redirect('/forgot');
        }
        res.render('reset', {
            user: req.user
        });
    });
});

app.post('/reset/:token', function (req, res) {
    async.waterfall([
        function (done) {
            User.findOne({
                resetPasswordToken: req.params.token,
                resetPasswordExpires: {$gt: Date.now()}
            }, function (err, user) {
                if (!user) {
                    req.flash('error', 'Password reset token is invalid or has expired.');
                    return res.redirect('back');
                }

                user.password = req.body.password;
                user.resetPasswordToken = undefined;
                user.resetPasswordExpires = undefined;

                user.save(function (err) {
                    req.logIn(user, function (err) {
                        done(err, user);
                    });
                });
            });
        },
        function (user, done) {
            var smtpTransport = nodemailer.createTransport('SMTP', {
                service: 'SendGrid',
                auth: {
                    user: '!!! YOUR SENDGRID USERNAME !!!',
                    pass: '!!! YOUR SENDGRID PASSWORD !!!'
                }
            });
            var mailOptions = {
                to: user.email,
                from: 'passwordreset@demo.com',
                subject: 'Your password has been changed',
                text: 'Hello,\n\n' +
                'This is a confirmation that the password for your account ' + user.email + ' has just been changed.\n'
            };
            smtpTransport.sendMail(mailOptions, function (err) {
                req.flash('success', 'Success! Your password has been changed.');
                done(err);
            });
        }
    ], function (err) {
        res.redirect('/');
    });
});


//album-----------------------------------------------
app.get('/home', requireLogin, function (req, res) {
    res.render('home.jade', {
        album: {
            albumName: 'Shahab'
        }
    });
});
app.get('/album', function (req, res) {


    res.render('album.jade', {
        album: {
            albumName: req.query.albumName
        }
    });

});

app.get('/gallery', requireLogin, function (req, res) {
   // console.log(req)
   // console.log(User.body.email);
    User.find({email: req.user.email}, {albumName: 1}, function (err, abc) {
       // console.log("err " + err);
        //console.log(abc[0]);


        Useralbum.find({} , {albumName:1 , file : 1},function(err , result1){


            //Useralbum.find({id:result.albumName} , {albumName:1},function(err , result1){
            //
            //    console.log("result " + result1);
            //    res.send(result1);
            //});
            //console.log("result " + result1);
            res.render('gallery.jade' ,{
                album:result1
            } );
        });
        //return callback(err, result);


      //  res.redirect('/gallery');
    });


    Useralbum.find({albumName: "albumcre"}, {file: 1},function(err, result){
        console.log(result);
        //callback(err,result);
        //handler: function(request, reply){
        console.log("successfully fetched ImageName"+result[0].file);
        name=result[0].file;
        //console.log(name);
        if(result[0].file==""){name='defaultImage.jpeg';
            console.log(name);
            reply.view("gallery.jade",{imageName:name});}
        else{res.render("gallery",{imageName:name});};
    })



});

//app.get('/album/:albumId',  requireLogin,  function(req, res){
//
//    Useralbum.findOneAndUpdate({_id: req.params.albumId},function (err, albumData) {
//        if(err){console.log("updatfile error>>"+err);}
//        else{console.log("updateresult")}
//        res.render('album.jade',{
//            album:{
//                albumName:
//            }
//        });
//
//
//    });
//});
app.post('/home', function (req, res) {
    //console.log(req.body);
    var album = new Useralbum({
        albumName: req.body.albumName,
        email: req.body.email
        //files:[]
    });
    console.log("a");
    console.log(album)
    album.save(function (err, data) {
        if (err) {
            var error = 'Something bad happened! Please try again.';
            res.render('home.jade', {error: error});
            //console.log(album);
        } else {
            User.findOneAndUpdate({email: req.body.email}, {$addToSet: {albumName: data._id}}, function (err, user) {
                if (err) {
                    console.log("updatfile error>>" + err);
                }
                else {
                    console.log("updateresult")
                }
                res.redirect('/album?albumName=' + req.body.albumName);
            });
        }
    });


});


var server = app.listen(3008, function () {

    var host = server.address().address;
    var port = server.address().port;
    console.log("Example app listening at http://%s:%s", host, port);

});

